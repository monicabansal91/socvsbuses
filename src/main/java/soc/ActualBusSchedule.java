package soc;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import au.com.bytecode.opencsv.CSVWriter;

public class ActualBusSchedule {
	static Calendar start = Calendar.getInstance();
	static Calendar end = Calendar.getInstance();


	public static void main(String[] args) throws Exception {
		List<String[]> allLinesCreated = new ArrayList<String[]>();
		String[] headers = {"route","day","time","value"};
		allLinesCreated.add(headers);

		prepareForBusSchedule(allLinesCreated, 7, 0, 10, 19, "A", "M", 11);
		prepareForBusSchedule(allLinesCreated, 10, 20, 20, 42, "A", "M", 7);
		prepareForBusSchedule(allLinesCreated, 0, 0, 2, 0, "B", "M", 30);
		prepareForBusSchedule(allLinesCreated, 6, 0, 7, 0, "B", "M", 30);
		prepareForBusSchedule(allLinesCreated, 7, 1, 8, 54, "B", "M", 7);
		prepareForBusSchedule(allLinesCreated, 8, 55, 12, 28, "B", "M", 5);
		prepareForBusSchedule(allLinesCreated, 12, 29, 19, 0, "B", "M", 6);
		prepareForBusSchedule(allLinesCreated, 19, 1, 20, 0, "B", "M", 7);
		prepareForBusSchedule(allLinesCreated, 20, 1, 22, 30, "B", "M", 15);
		prepareForBusSchedule(allLinesCreated, 22, 31, 23, 59, "B", "M", 30);
		prepareForBusSchedule(allLinesCreated, 7, 30, 21, 54, "C", "M", 12);
		prepareForBusSchedule(allLinesCreated, 0, 0, 2, 36, "EE", "M", 18);
		prepareForBusSchedule(allLinesCreated, 6, 0, 7, 0, "EE", "M", 20);
		prepareForBusSchedule(allLinesCreated, 7, 1, 10, 35, "EE", "M", 11);
		prepareForBusSchedule(allLinesCreated, 10, 36, 19, 45, "EE", "M", 9);
		prepareForBusSchedule(allLinesCreated, 19, 46, 23, 0, "EE", "M", 15);
		prepareForBusSchedule(allLinesCreated, 23, 1, 23, 59, "EE", "M", 18);
		prepareForBusSchedule(allLinesCreated, 7, 0, 9, 0, "F", "M", 8);
		prepareForBusSchedule(allLinesCreated, 9, 1, 11, 50, "F", "M", 7);
		prepareForBusSchedule(allLinesCreated, 11, 51, 16, 6, "F", "M", 6);
		prepareForBusSchedule(allLinesCreated, 16, 7, 19, 21, "F", "M", 7);
		prepareForBusSchedule(allLinesCreated, 19, 22, 20, 58, "F", "M", 7);
		prepareForBusSchedule(allLinesCreated, 0, 0, 2, 48, "H", "M", 18);
		prepareForBusSchedule(allLinesCreated, 6, 0, 7, 0, "H", "M", 18);
		prepareForBusSchedule(allLinesCreated, 7, 1, 9, 9, "H", "M", 12);
		prepareForBusSchedule(allLinesCreated, 9, 10, 21, 0, "H", "M", 9);
		prepareForBusSchedule(allLinesCreated, 21, 1, 22, 54, "H", "M", 12);
		prepareForBusSchedule(allLinesCreated, 22, 55, 23, 59, "H", "M", 18);
		prepareForBusSchedule(allLinesCreated, 0, 0, 2, 16, "LX", "M", 15);
		prepareForBusSchedule(allLinesCreated, 6, 0, 7, 0, "LX", "M", 15);
		prepareForBusSchedule(allLinesCreated, 7, 1, 10, 30, "LX", "M", 6);
		prepareForBusSchedule(allLinesCreated, 10, 31, 12, 34, "LX", "M", 4);
		prepareForBusSchedule(allLinesCreated, 12, 35, 20, 4, "LX", "M", 4);
		prepareForBusSchedule(allLinesCreated, 20, 5, 22, 16, "LX", "M", 8);
		prepareForBusSchedule(allLinesCreated, 22, 17, 23, 59, "LX", "M", 15);
		prepareForBusSchedule(allLinesCreated, 7, 0, 12, 48, "REXB", "M", 12);
		prepareForBusSchedule(allLinesCreated, 12, 49, 23, 0, "REXB", "M", 9);
		prepareForBusSchedule(allLinesCreated, 7, 0, 9, 0, "REXL", "M", 12);
		prepareForBusSchedule(allLinesCreated, 9, 1, 12, 36, "REXL", "M", 9);
		prepareForBusSchedule(allLinesCreated, 12, 37, 19, 1, "REXL", "M", 7);
		prepareForBusSchedule(allLinesCreated, 19, 2, 23, 4, "REXL", "M", 8);
		prepareForBusSchedule(allLinesCreated, 7, 0, 10, 19, "A", "T", 11);
		prepareForBusSchedule(allLinesCreated, 10, 20, 20, 42, "A", "T", 7);
		prepareForBusSchedule(allLinesCreated, 0, 0, 2, 0, "B", "T", 30);
		prepareForBusSchedule(allLinesCreated, 6, 0, 7, 0, "B", "T", 30);
		prepareForBusSchedule(allLinesCreated, 7, 1, 8, 54, "B", "T", 7);
		prepareForBusSchedule(allLinesCreated, 8, 55, 12, 28, "B", "T", 5);
		prepareForBusSchedule(allLinesCreated, 12, 29, 19, 0, "B", "T", 6);
		prepareForBusSchedule(allLinesCreated, 19, 1, 20, 0, "B", "T", 7);
		prepareForBusSchedule(allLinesCreated, 20, 1, 22, 30, "B", "T", 15);
		prepareForBusSchedule(allLinesCreated, 22, 31, 23, 59, "B", "T", 30);
		prepareForBusSchedule(allLinesCreated, 7, 30, 21, 54, "C", "T", 12);
		prepareForBusSchedule(allLinesCreated, 0, 0, 2, 36, "EE", "T", 18);
		prepareForBusSchedule(allLinesCreated, 6, 0, 7, 0, "EE", "T", 20);
		prepareForBusSchedule(allLinesCreated, 7, 1, 10, 35, "EE", "T", 11);
		prepareForBusSchedule(allLinesCreated, 10, 36, 19, 45, "EE", "T", 9);
		prepareForBusSchedule(allLinesCreated, 19, 46, 23, 0, "EE", "T", 15);
		prepareForBusSchedule(allLinesCreated, 23, 1, 23, 59, "EE", "T", 18);
		prepareForBusSchedule(allLinesCreated, 7, 0, 9, 0, "F", "T", 8);
		prepareForBusSchedule(allLinesCreated, 9, 1, 11, 50, "F", "T", 7);
		prepareForBusSchedule(allLinesCreated, 11, 51, 16, 6, "F", "T", 6);
		prepareForBusSchedule(allLinesCreated, 16, 7, 19, 21, "F", "T", 7);
		prepareForBusSchedule(allLinesCreated, 19, 22, 20, 58, "F", "T", 7);
		prepareForBusSchedule(allLinesCreated, 0, 0, 2, 48, "H", "T", 18);
		prepareForBusSchedule(allLinesCreated, 6, 0, 7, 0, "H", "T", 18);
		prepareForBusSchedule(allLinesCreated, 7, 1, 9, 9, "H", "T", 12);
		prepareForBusSchedule(allLinesCreated, 9, 10, 21, 0, "H", "T", 9);
		prepareForBusSchedule(allLinesCreated, 21, 1, 22, 54, "H", "T", 12);
		prepareForBusSchedule(allLinesCreated, 22, 55, 23, 59, "H", "T", 18);
		prepareForBusSchedule(allLinesCreated, 0, 0, 2, 16, "LX", "T", 15);
		prepareForBusSchedule(allLinesCreated, 6, 0, 7, 0, "LX", "T", 15);
		prepareForBusSchedule(allLinesCreated, 7, 1, 10, 30, "LX", "T", 6);
		prepareForBusSchedule(allLinesCreated, 10, 31, 12, 34, "LX", "T", 4);
		prepareForBusSchedule(allLinesCreated, 12, 35, 20, 4, "LX", "T", 4);
		prepareForBusSchedule(allLinesCreated, 20, 5, 22, 16, "LX", "T", 8);
		prepareForBusSchedule(allLinesCreated, 22, 17, 23, 59, "LX", "T", 15);
		prepareForBusSchedule(allLinesCreated, 7, 0, 12, 48, "REXB", "T", 12);
		prepareForBusSchedule(allLinesCreated, 12, 49, 23, 0, "REXB", "T", 9);
		prepareForBusSchedule(allLinesCreated, 7, 0, 9, 0, "REXL", "T", 12);
		prepareForBusSchedule(allLinesCreated, 9, 1, 12, 36, "REXL", "T", 9);
		prepareForBusSchedule(allLinesCreated, 12, 37, 19, 1, "REXL", "T", 7);
		prepareForBusSchedule(allLinesCreated, 19, 2, 23, 4, "REXL", "T", 8);
		prepareForBusSchedule(allLinesCreated, 7, 0, 10, 19, "A", "W", 11);
		prepareForBusSchedule(allLinesCreated, 10, 20, 20, 42, "A", "W", 7);
		prepareForBusSchedule(allLinesCreated, 0, 0, 2, 0, "B", "W", 30);
		prepareForBusSchedule(allLinesCreated, 6, 0, 7, 0, "B", "W", 30);
		prepareForBusSchedule(allLinesCreated, 7, 1, 8, 54, "B", "W", 7);
		prepareForBusSchedule(allLinesCreated, 8, 55, 12, 28, "B", "W", 5);
		prepareForBusSchedule(allLinesCreated, 12, 29, 19, 0, "B", "W", 6);
		prepareForBusSchedule(allLinesCreated, 19, 1, 20, 0, "B", "W", 7);
		prepareForBusSchedule(allLinesCreated, 20, 1, 22, 30, "B", "W", 15);
		prepareForBusSchedule(allLinesCreated, 22, 31, 23, 59, "B", "W", 30);
		prepareForBusSchedule(allLinesCreated, 7, 30, 21, 54, "C", "W", 12);
		prepareForBusSchedule(allLinesCreated, 0, 0, 2, 36, "EE", "W", 18);
		prepareForBusSchedule(allLinesCreated, 6, 0, 7, 0, "EE", "W", 20);
		prepareForBusSchedule(allLinesCreated, 7, 1, 10, 35, "EE", "W", 11);
		prepareForBusSchedule(allLinesCreated, 10, 36, 19, 45, "EE", "W", 9);
		prepareForBusSchedule(allLinesCreated, 19, 46, 23, 0, "EE", "W", 15);
		prepareForBusSchedule(allLinesCreated, 23, 1, 23, 59, "EE", "W", 18);
		prepareForBusSchedule(allLinesCreated, 7, 0, 9, 0, "F", "W", 8);
		prepareForBusSchedule(allLinesCreated, 9, 1, 11, 50, "F", "W", 7);
		prepareForBusSchedule(allLinesCreated, 11, 51, 16, 6, "F", "W", 6);
		prepareForBusSchedule(allLinesCreated, 16, 7, 19, 21, "F", "W", 7);
		prepareForBusSchedule(allLinesCreated, 19, 22, 20, 58, "F", "W", 7);
		prepareForBusSchedule(allLinesCreated, 0, 0, 2, 48, "H", "W", 18);
		prepareForBusSchedule(allLinesCreated, 6, 0, 7, 0, "H", "W", 18);
		prepareForBusSchedule(allLinesCreated, 7, 1, 9, 9, "H", "W", 12);
		prepareForBusSchedule(allLinesCreated, 9, 10, 21, 0, "H", "W", 9);
		prepareForBusSchedule(allLinesCreated, 21, 1, 22, 54, "H", "W", 12);
		prepareForBusSchedule(allLinesCreated, 22, 55, 23, 59, "H", "W", 18);
		prepareForBusSchedule(allLinesCreated, 0, 0, 2, 16, "LX", "W", 15);
		prepareForBusSchedule(allLinesCreated, 6, 0, 7, 0, "LX", "W", 15);
		prepareForBusSchedule(allLinesCreated, 7, 1, 10, 30, "LX", "W", 6);
		prepareForBusSchedule(allLinesCreated, 10, 31, 12, 34, "LX", "W", 4);
		prepareForBusSchedule(allLinesCreated, 12, 35, 20, 4, "LX", "W", 4);
		prepareForBusSchedule(allLinesCreated, 20, 5, 22, 16, "LX", "W", 8);
		prepareForBusSchedule(allLinesCreated, 22, 17, 23, 59, "LX", "W", 15);
		prepareForBusSchedule(allLinesCreated, 7, 0, 12, 48, "REXB", "W", 12);
		prepareForBusSchedule(allLinesCreated, 12, 49, 23, 0, "REXB", "W", 9);
		prepareForBusSchedule(allLinesCreated, 7, 0, 9, 0, "REXL", "W", 12);
		prepareForBusSchedule(allLinesCreated, 9, 1, 12, 36, "REXL", "W", 9);
		prepareForBusSchedule(allLinesCreated, 12, 37, 19, 1, "REXL", "W", 7);
		prepareForBusSchedule(allLinesCreated, 19, 2, 23, 4, "REXL", "W", 8);
		prepareForBusSchedule(allLinesCreated, 7, 0, 10, 19, "A", "TH", 12);
		prepareForBusSchedule(allLinesCreated, 10, 20, 20, 42, "A", "TH", 7);
		prepareForBusSchedule(allLinesCreated, 0, 0, 2, 0, "B", "TH", 30);
		prepareForBusSchedule(allLinesCreated, 6, 0, 7, 0, "B", "TH", 30);
		prepareForBusSchedule(allLinesCreated, 7, 1, 8, 54, "B", "TH", 6);
		prepareForBusSchedule(allLinesCreated, 8, 55, 12, 28, "B", "TH", 6);
		prepareForBusSchedule(allLinesCreated, 12, 29, 19, 0, "B", "TH", 5);
		prepareForBusSchedule(allLinesCreated, 19, 1, 20, 0, "B", "TH", 7);
		prepareForBusSchedule(allLinesCreated, 20, 1, 22, 30, "B", "TH", 15);
		prepareForBusSchedule(allLinesCreated, 22, 31, 23, 59, "B", "TH", 30);
		prepareForBusSchedule(allLinesCreated, 7, 30, 21, 54, "C", "TH", 12);
		prepareForBusSchedule(allLinesCreated, 0, 0, 2, 30, "EE", "TH", 18);
		prepareForBusSchedule(allLinesCreated, 6, 0, 7, 0, "EE", "TH", 20);
		prepareForBusSchedule(allLinesCreated, 7, 1, 10, 35, "EE", "TH", 12);
		prepareForBusSchedule(allLinesCreated, 10, 36, 19, 45, "EE", "TH", 9);
		prepareForBusSchedule(allLinesCreated, 19, 46, 23, 0, "EE", "TH", 15);
		prepareForBusSchedule(allLinesCreated, 23, 1, 23, 59, "EE", "TH", 18);
		prepareForBusSchedule(allLinesCreated, 7, 0, 9, 0, "F", "TH", 5);
		prepareForBusSchedule(allLinesCreated, 9, 1, 11, 50, "F", "TH", 7);
		prepareForBusSchedule(allLinesCreated, 11, 51, 16, 6, "F", "TH", 6);
		prepareForBusSchedule(allLinesCreated, 16, 7, 19, 21, "F", "TH", 7);
		prepareForBusSchedule(allLinesCreated, 19, 22, 20, 58, "F", "TH", 7);
		prepareForBusSchedule(allLinesCreated, 0, 0, 2, 48, "H", "TH", 18);
		prepareForBusSchedule(allLinesCreated, 6, 0, 7, 0, "H", "TH", 18);
		prepareForBusSchedule(allLinesCreated, 7, 1, 9, 9, "H", "TH", 12);
		prepareForBusSchedule(allLinesCreated, 9, 10, 21, 0, "H", "TH", 9);
		prepareForBusSchedule(allLinesCreated, 21, 1, 22, 54, "H", "TH", 12);
		prepareForBusSchedule(allLinesCreated, 22, 55, 23, 59, "H", "TH", 18);
		prepareForBusSchedule(allLinesCreated, 0, 0, 2, 16, "LX", "TH", 15);
		prepareForBusSchedule(allLinesCreated, 6, 0, 7, 0, "LX", "TH", 15);
		prepareForBusSchedule(allLinesCreated, 7, 1, 10, 30, "LX", "TH", 6);
		prepareForBusSchedule(allLinesCreated, 10, 31, 12, 34, "LX", "TH", 4);
		prepareForBusSchedule(allLinesCreated, 12, 35, 20, 4, "LX", "TH", 4);
		prepareForBusSchedule(allLinesCreated, 20, 5, 22, 16, "LX", "TH", 8);
		prepareForBusSchedule(allLinesCreated, 22, 17, 23, 59, "LX", "TH", 15);
		prepareForBusSchedule(allLinesCreated, 7, 0, 12, 48, "REXB", "TH", 12);
		prepareForBusSchedule(allLinesCreated, 12, 49, 23, 0, "REXB", "TH", 9);
		prepareForBusSchedule(allLinesCreated, 7, 0, 9, 0, "REXL", "TH", 12);
		prepareForBusSchedule(allLinesCreated, 9, 1, 12, 36, "REXL", "TH", 9);
		prepareForBusSchedule(allLinesCreated, 12, 37, 19, 1, "REXL", "TH", 7);
		prepareForBusSchedule(allLinesCreated, 19, 2, 23, 4, "REXL", "TH", 9);
		prepareForBusSchedule(allLinesCreated, 7, 0, 10, 19, "A", "F", 12);
		prepareForBusSchedule(allLinesCreated, 10, 20, 14, 3, "A", "F", 7);
		prepareForBusSchedule(allLinesCreated, 14, 4, 20, 57, "A", "F", 9);
		prepareForBusSchedule(allLinesCreated, 0, 0, 2, 0, "B", "F", 30);
		prepareForBusSchedule(allLinesCreated, 6, 0, 7, 0, "B", "F", 30);
		prepareForBusSchedule(allLinesCreated, 7, 1, 8, 54, "B", "F", 7);
		prepareForBusSchedule(allLinesCreated, 8, 55, 20, 0, "B", "F", 7);
		prepareForBusSchedule(allLinesCreated, 20, 1, 22, 30, "B", "F", 15);
		prepareForBusSchedule(allLinesCreated, 22, 31, 23, 59, "B", "F", 30);
		prepareForBusSchedule(allLinesCreated, 7, 30, 19, 00, "C", "F", 12);
		prepareForBusSchedule(allLinesCreated, 0, 0, 2, 30, "EE", "F", 18);
		prepareForBusSchedule(allLinesCreated, 6, 0, 7, 0, "EE", "F", 20);
		prepareForBusSchedule(allLinesCreated, 7, 1, 10, 35, "EE", "F", 12);
		prepareForBusSchedule(allLinesCreated, 10, 36, 19, 45, "EE", "F", 9);
		prepareForBusSchedule(allLinesCreated, 19, 46, 23, 0, "EE", "F", 15);
		prepareForBusSchedule(allLinesCreated, 23, 1, 23, 59, "EE", "F", 18);
		prepareForBusSchedule(allLinesCreated, 7, 0, 9, 0, "F", "F", 8);
		prepareForBusSchedule(allLinesCreated, 9, 1, 16, 6, "F", "F", 7);
		prepareForBusSchedule(allLinesCreated, 16, 7, 19, 21, "F", "F", 8);
		prepareForBusSchedule(allLinesCreated, 19, 22, 20, 58, "F", "F", 9);
		prepareForBusSchedule(allLinesCreated, 0, 0, 2, 48, "H", "F", 18);
		prepareForBusSchedule(allLinesCreated, 6, 0, 7, 0, "H", "F", 18);
		prepareForBusSchedule(allLinesCreated, 7, 1, 9, 9, "H", "F", 12);
		prepareForBusSchedule(allLinesCreated, 9, 10, 21, 0, "H", "F", 9);
		prepareForBusSchedule(allLinesCreated, 21, 1, 22, 54, "H", "F", 12);
		prepareForBusSchedule(allLinesCreated, 22, 55, 23, 59, "H", "F", 18);
		prepareForBusSchedule(allLinesCreated, 0, 0, 2, 16, "LX", "F", 15);
		prepareForBusSchedule(allLinesCreated, 6, 0, 7, 0, "LX", "F", 15);
		prepareForBusSchedule(allLinesCreated, 7, 1, 10, 30, "LX", "F", 6);
		prepareForBusSchedule(allLinesCreated, 10, 31, 12, 34, "LX", "F", 5);
		prepareForBusSchedule(allLinesCreated, 12, 35, 20, 4, "LX", "F", 6);
		prepareForBusSchedule(allLinesCreated, 20, 5, 22, 16, "LX", "F", 10);
		prepareForBusSchedule(allLinesCreated, 22, 17, 23, 59, "LX", "F", 15);
		prepareForBusSchedule(allLinesCreated, 7, 0, 15, 24, "REXB", "F", 12);
		prepareForBusSchedule(allLinesCreated, 15, 25, 23, 0, "REXB", "F", 18);
		prepareForBusSchedule(allLinesCreated, 7, 0, 9, 0, "REXL", "F", 12);
		prepareForBusSchedule(allLinesCreated, 9, 1, 12, 36, "REXL", "F", 9);
		prepareForBusSchedule(allLinesCreated, 12, 37, 15, 17, "REXL", "F", 7);
		prepareForBusSchedule(allLinesCreated, 15, 18, 18, 29, "REXL", "F", 12);
		prepareForBusSchedule(allLinesCreated, 19, 2, 22, 0, "REXL", "F", 18);

		writeCSV("busesExpected", allLinesCreated);
	}

	private static void prepareForBusSchedule(List<String[]> allLinesCreated, int startHour, int startMinute, int endHour, int endMinute, String route, String day, int noOfBuses) {
		start.clear();
		start.set(Calendar.HOUR_OF_DAY, startHour);
		start.set(Calendar.MINUTE, startMinute);

		end.clear();
		end.set(Calendar.HOUR_OF_DAY, endHour);
		end.set(Calendar.MINUTE, endMinute);

		allLinesCreated.addAll(createPerMinuteSchedule(start, end, route, day, noOfBuses));
	}

	public static List<String[]> createPerMinuteSchedule(Calendar startTime, Calendar endTime, String route, String day, int noOfBuses) {
		List<String[]> linesCreated = new ArrayList<String[]>();
		while(!startTime.after(endTime)) {
			String[] line = {route, day, ""+startTime.getTimeInMillis(), ""+noOfBuses};
			linesCreated.add(line);
			startTime.add(Calendar.MINUTE, 1);
		}
		return linesCreated;
	}

	public static void writeCSV(String fileName, List<String[]> lines) throws Exception
	{
		String csv = fileName + ".csv";
		CSVWriter writer = new CSVWriter(new FileWriter(csv));
		writer.writeAll(lines);
		writer.close();
	}

}
