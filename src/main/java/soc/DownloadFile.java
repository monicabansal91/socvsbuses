package soc;

import java.io.File;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

public class DownloadFile {

	public static void main(String[] args) {
		// http://sis.rutgers.edu/soc/api/courses.gzip?year=2017&term=9&campus=NB
		
		try {
			FileUtils.copyURLToFile(new URL("http://sis.rutgers.edu/soc/api/courses.gzip?year=2017&term=9&campus=NB"), new File("allCourses.json"));
			String contents = new String(Files.readAllBytes(Paths.get("allCourses.json")));
			contents = contents.substring(1, contents.length()-1);
			contents = contents.replace(",{\"campusLocations\":", "\n{\"campusLocations\":");
			//Files.write(Paths.get("editedForMongo.json"), contents.getBytes());
			String[] split = contents.split("\n");
			List<DBObject> allEntries = new ArrayList<>();
			for (String entry : split) {
				DBObject dbObject = (DBObject) com.mongodb.util.JSON.parse(entry);
				allEntries.add(dbObject);
			}
			MongoClient mongoClient = new MongoClient();
			DB rutgersSoc = mongoClient.getDB("otherTest");
			DBCollection courses = rutgersSoc.getCollection("courses");
			courses.drop();
			courses.insert(allEntries);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
