package soc;

import java.io.FileWriter;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import au.com.bytecode.opencsv.CSVWriter;

public class ObservedBusSchedule {
	public static final String COMPLETED_PREDICTIONS_FOR_MULTIPLE_BUS_STOPS = "http://webservices.nextbus.com/service/publicXMLFeed?a=rutgers&command=predictionsForMultiStops&stops=a||rutgerss_a&stops=a||hillw&stops=a||stuactcntr&stops=a||werblinm&stops=a||libofsciw&stops=a||science&stops=a||busch_a&stops=a||werblinback&stops=a||scott&stops=a||stadium_a&stops=a||buells&stops=a||lot48a&stops=a||buschse&stops=b||libofsci&stops=b||hillw&stops=b||beck&stops=b||quads&stops=b||science&stops=b||busch_a&stops=b||buschse&stops=b||werblinback&stops=b||livingston_a&stops=c||hillw&stops=c||stadium_a&stops=c||allison&stops=c||hilln&stops=c||werblinback&stops=ee||rutgerss_a&stops=ee||biel&stops=ee||foodsci&stops=ee||stuactcntr&stops=ee||gibbons&stops=ee||rockoff&stops=ee||traine&stops=ee||lipman&stops=ee||pubsafs&stops=ee||college_a&stops=ee||patersonn&stops=ee||patersons&stops=ee||katzenbach&stops=ee||scott&stops=ee||pubsafn&stops=ee||henders&stops=ee||zimmerli&stops=ee||redoak_a&stops=ee||liberty&stops=ee||trainn_a&stops=f||rutgerss_a&stops=f||katzenbach&stops=f||biel&stops=f||foodsci&stops=f||stuactcntr&stops=f||redoak&stops=f||gibbons&stops=f||lipman&stops=f||scott&stops=f||pubsafs&stops=f||henders&stops=f||college_a&stops=h||rutgerss_a&stops=h||libofsci&stops=h||stuactcntr&stops=h||werblinm&stops=h||traine&stops=h||davidson&stops=h||scott&stops=h||stadium_a&stops=h||buel&stops=h||busch_a&stops=h||allison&stops=h||hilln&stops=lx||rutgerss_a&stops=lx||stuactcntr&stops=lx||beck&stops=lx||quads&stops=lx||traine&stops=lx||scott&stops=lx||livingston_a&stops=rexb||hillw&stops=rexb||allison_a&stops=rexb||lipman&stops=rexb||pubsafs&stops=rexb||rockhall&stops=rexb||redoak_a&stops=rexb||college_a&stops=rexb||hilln&stops=rexb||newstree&stops=rexl||beck&stops=rexl||lipman&stops=rexl||pubsafs&stops=rexl||rockhall&stops=rexl||redoak_a&stops=rexl||college_a&stops=rexl||newstree&stops=rexl||livingston_a&stops=s||biel&stops=s||stuactcntrs&stops=s||libofsciw&stops=s||lipman&stops=s||pubsafs&stops=s||college_a&stops=s||werblinback&stops=s||patersonn&stops=s||quads&stops=s||pubsafn&stops=s||zimmerli_2&stops=s||buschse&stops=s||trainn_a&stops=s||traine_a&stops=s||rutgerss_a&stops=s||hillw&stops=s||stuactcntr&stops=s||foodsci&stops=s||gibbons&stops=s||science&stops=s||busch_a&stops=s||livingston_a&stops=s||katzenbach&stops=s||beck&stops=s||scott&stops=s||henders&stops=s||redoak_a&stops=s||lot48a&stops=s||liberty&stops=s||stuactcntrn_2&stops=w1||rutgerss_a&stops=w1||traine_a&stops=w1||colony&stops=w1||scott&stops=w1||nursscho&stops=w2||rutgerss_a&stops=w2||traine_a&stops=w2||stuactcntr&stops=w2||colony&stops=w2||trainn&stops=w2||nursscho&stops=w2||zimmerli&stops=wknd1||livingston&stops=wknd1||rutgerss&stops=wknd1||biel&stops=wknd1||stuactcntrs&stops=wknd1||traine&stops=wknd1||libofsciw&stops=wknd1||lipman&stops=wknd1||pubsafs&stops=wknd1||busch&stops=wknd1||werblinback&stops=wknd1||patersonn&stops=wknd1||quads&stops=wknd1||pubsafn&stops=wknd1||zimmerli_2&stops=wknd1||buschse&stops=wknd1||college&stops=wknd1||hillw&stops=wknd1||stuactcntr&stops=wknd1||foodsci&stops=wknd1||gibbons&stops=wknd1||science&stops=wknd1||trainn&stops=wknd1||katzenbach&stops=wknd1||beck&stops=wknd1||redoak&stops=wknd1||scott&stops=wknd1||henders&stops=wknd1||lot48a&stops=wknd1||liberty&stops=wknd1||stuactcntrn_2&stops=wknd2||livingston&stops=wknd2||rutgerss&stops=wknd2||libofsci&stops=wknd2||biel&stops=wknd2||foodsci&stops=wknd2||gibbons&stops=wknd2||rockoff&stops=wknd2||traine&stops=wknd2||lipman&stops=wknd2||pubsafs&stops=wknd2||stuactcntrn&stops=wknd2||busch&stops=wknd2||hilln&stops=wknd2||patersons&stops=wknd2||katzenbach&stops=wknd2||redoak&stops=wknd2||beck&stops=wknd2||quads&stops=wknd2||davidson&stops=wknd2||scott&stops=wknd2||henders&stops=wknd2||college&stops=wknd2||allison&stops=rbhs||clinacad";
	
	public static Map<String, Set<String>> getRouteVehicleMap() {
		Map<String, Set<String>> routeVehicles = new HashMap<>();
		Document document = HTTPListener.getDocument(COMPLETED_PREDICTIONS_FOR_MULTIPLE_BUS_STOPS);
		NodeList predictions = document.getElementsByTagName("predictions");
		for(int i = 0 ; i < predictions.getLength() ; i++) {
			Element prediction = (Element) predictions.item(i);
			String busStopTag = prediction.getAttribute("stopTag");
			String routeTag = prediction.getAttribute("routeTag");
			if(!routeVehicles.containsKey(routeTag)) {
				routeVehicles.put(routeTag, new HashSet<>());
			}
			NodeList directions = prediction.getElementsByTagName("direction");
			for(int j = 0 ; j < directions.getLength() ; j++) {
				Element directionElement = (Element) directions.item(j);
				NodeList predictionList = directionElement.getElementsByTagName("prediction");
				for(int k = 0 ; k < predictionList.getLength() ; k++) {
					Element predElement = (Element) predictionList.item(k);
					String vehicle = predElement.getAttribute("vehicle");
					routeVehicles.get(routeTag).add(vehicle);
				}
			}
		}
		System.out.println(routeVehicles);
		return routeVehicles;
	}
	
	public static void main(String[] args) throws Exception {
		//startListener();
		//retrieveAllRecordedTimes();
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(Calendar.HOUR_OF_DAY, 8);
		
		System.out.println(calendar.getTimeInMillis());
	}
	
	public static void retrieveAllRecordedTimes() throws Exception {
		MongoClient mongoClient = new MongoClient();
		DB rutgersSoc = mongoClient.getDB("otherTest");
		DBCollection buses = rutgersSoc.getCollection("buses");
		
		DBCursor list = buses.find();
		
		List<String[]> linesCreated = new ArrayList<String[]>();
		String[] headers = {"route","day","time","value"};
		linesCreated.add(headers);
		
		for (DBObject dbObject : list) {
			String route = (String)dbObject.get("route");
			Integer vehicleCount = (Integer)dbObject.get("vehicleCount");
			Long time = (Long)dbObject.get("time");
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(time);
			String dayOfWeek = (String) dbObject.get("dayOfWeek");
			
			String[] line = createBusLineForCSV(calendar, route, dayOfWeek, vehicleCount);
			linesCreated.add(line);
		}
		
		writeCSV("actual", linesCreated);
	}

	private static void startListener() {
		Calendar epoch = Calendar.getInstance();
		epoch.clear();
		String[] days = {"","SU","M","T","W","TH","F","S"};
		TimerTask timerTask = new TimerTask() {
			
			@Override
			public void run() {
				try {
					Calendar calendar = Calendar.getInstance();
					String dayOfWeek = days[calendar.get(Calendar.DAY_OF_WEEK)];
					calendar.set(epoch.get(Calendar.YEAR), epoch.get(Calendar.MONTH), epoch.get(Calendar.DAY_OF_MONTH));
					calendar.set(Calendar.SECOND, epoch.get(Calendar.SECOND));
					calendar.set(Calendar.MILLISECOND, epoch.get(Calendar.MILLISECOND));
					Map<String, Set<String>> routeVehicleMap = getRouteVehicleMap();
					List<DBObject> allEntries = new ArrayList<>();
					for (String key : routeVehicleMap.keySet()) {
						Set<String> vehicles = routeVehicleMap.get(key);
						DBObject bdo = new BasicDBObject();
						bdo.put("route", key);
						bdo.put("vehicleCount", vehicles.size());
						bdo.put("time", calendar.getTimeInMillis());
						bdo.put("dayOfWeek", dayOfWeek);
						allEntries.add(bdo);
					}
					MongoClient mongoClient = new MongoClient();
					DB rutgersSoc = mongoClient.getDB("otherTest");
					DBCollection buses = rutgersSoc.getCollection("buses");
					buses.insert(allEntries);
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
		Timer timer = new Timer();
		timer.schedule(timerTask, 0, 60*1000);
	}

	public static List<String[]> createPerMinuteSchedule(Calendar startTime, Calendar endTime, String route, String day, int noOfBuses) {
		List<String[]> linesCreated = new ArrayList<String[]>();
		while(!startTime.after(endTime)) {
			String[] line = createBusLineForCSV(startTime, route, day, noOfBuses);
			linesCreated.add(line);
			startTime.add(Calendar.MINUTE, 1);
		}
		return linesCreated;
	}

	private static String[] createBusLineForCSV(Calendar startTime, String route, String day, int noOfBuses) {
		String[] line = {route, day, ""+startTime.getTimeInMillis(), ""+noOfBuses};
		return line;
	}

	public static void writeCSV(String fileName, List<String[]> lines) throws Exception
	{
		String csv = fileName + ".csv";
		CSVWriter writer = new CSVWriter(new FileWriter(csv));
		writer.writeAll(lines);
		writer.close();
	}

}
