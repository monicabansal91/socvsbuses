package soc;

import java.io.FileWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import au.com.bytecode.opencsv.CSVWriter;

public class SOCPreProcessing {

	/***
	 * Normal Distribution for people coming in lecture 
	 * 0.00013383	-14
		0.004431848	-13
		0.053990967	-12
		0.241970725	-11
		0.39894228	-10
		0.241970725	-9
		0.053990967	-8
		0.004431848	-7
		0.00013383	-6
	 * @param args
	 * @throws Exception
	 */

	@SuppressWarnings("unused")
	public static void main(String[] args) throws Exception {
		Map<String, String> campusDictionary = new HashMap<>();
		campusDictionary.put("1", "College Avenue");
		campusDictionary.put("2", "Busch");
		campusDictionary.put("3", "Livingston");
		campusDictionary.put("4", "Cook Douglas");
		Map<Integer, Double> normDistForArr = new HashMap<Integer, Double>();
		normDistForArr.put(-14, 0.00013383);
		normDistForArr.put(-13, 0.004431848);
		normDistForArr.put(-12, 0.053990967);
		normDistForArr.put(-11, 0.241970725);
		normDistForArr.put(-10, 0.39894228);
		normDistForArr.put(-9, 0.241970725);
		normDistForArr.put(-8, 0.053990967);
		normDistForArr.put(-7, 0.004431848);
		normDistForArr.put(-6, 0.00013383);

		Map<String, Map> campusWiseMap = new HashMap<String, Map>();
		//Map<String, TreeMap<Calendar, Double>> all = new HashMap<String, TreeMap<Calendar, Double>>();
		MongoClient mongoClient = new MongoClient();
		DB rutgersSoc = mongoClient.getDB("rutgers_soc");
		DBCollection courses = rutgersSoc.getCollection("courses");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("courseString", new BasicDBObject("$ne", null));
		map.put("sections.meetingTimes.startTime", new BasicDBObject("$ne", null));
		map.put("sections.meetingTimes.endTime", new BasicDBObject("$ne", null));
		map.put("sections.meetingTimes.campusLocation", new BasicDBObject("$ne", null));
		map.put("sections.meetingTimes.pmCode", new BasicDBObject("$ne", null));
		map.put("sections.meetingTimes.meetingDay", new BasicDBObject("$ne", null));
		DBObject ref = new BasicDBObject(map);
		DBCursor results = courses.find(ref);
		System.out.println(results.count());
		for(DBObject result : results) {
			BasicDBList sections = (BasicDBList) result.get("sections");
			for(Object section : sections) {
				DBObject sectionDBO = (DBObject)section;
				BasicDBList meetingTimes = (BasicDBList) sectionDBO.get("meetingTimes");
				for (Object meetingTime : meetingTimes) {
					DBObject meetingTimeDBO = (DBObject)meetingTime;
					String day = (String)meetingTimeDBO.get("meetingDay");
					//Insert campus differentiator here
					String campusLocationId = (String) meetingTimeDBO.get("campusLocation");
					if(!campusWiseMap.containsKey(campusLocationId)) {
						campusWiseMap.put(campusLocationId, new HashMap<String, TreeMap<Calendar, Double>>());
					}
					Map<String, TreeMap<Calendar, Double>> all = campusWiseMap.get(campusLocationId);
					if(!all.containsKey(day)) {
						all.put(day, new TreeMap<Calendar, Double>());
					}
					TreeMap<Calendar, Double> dayMap = all.get(day);
					Integer startTime = Integer.parseInt((String) meetingTimeDBO.get("startTimeMilitary"));
					for (Entry<Integer, Double> nd : normDistForArr.entrySet()) {
						//int arrTime = startTime-nd.getKey();
						Calendar calendar = Calendar.getInstance();
						calendar.clear();
						calendar.set(Calendar.HOUR_OF_DAY, startTime/100);
						calendar.set(Calendar.MINUTE, startTime%100);
						calendar.add(Calendar.MINUTE, nd.getKey());
						if (!dayMap.containsKey(calendar)) {
							dayMap.put(calendar, 0.0);
						}
						dayMap.put(calendar, dayMap.get(calendar) + nd.getValue());
					}
					Integer endTime = Integer.parseInt((String) meetingTimeDBO.get("endTimeMilitary"));
					for (Entry<Integer, Double> nd : normDistForArr.entrySet()) {
						//int arrTime = startTime-nd.getKey();
						Calendar calendar = Calendar.getInstance();
						calendar.clear();
						calendar.set(Calendar.HOUR_OF_DAY, endTime/100);
						calendar.set(Calendar.MINUTE, ((endTime%100)+10));
						calendar.add(Calendar.MINUTE, nd.getKey());
						if (!dayMap.containsKey(calendar)) {
							dayMap.put(calendar, 0.0);
						}
						dayMap.put(calendar, dayMap.get(calendar) + nd.getValue());
					}
				}
			}
		}
		String[] headers = {"campus","day","time","value"};
		List<String[]> allLines = new ArrayList<>();
		allLines.add(headers);
		DecimalFormat df = new DecimalFormat("#.####");
		for (Entry<String, Map> entry  : campusWiseMap.entrySet()) {
			System.out.println("\n\n\n"+entry.getKey());
			Map<String, TreeMap<Calendar, Double>> all = entry.getValue();
			for (Entry<String, TreeMap<Calendar, Double>> dayMap : all.entrySet()) {
				System.out.println(dayMap.getKey());
				for (Entry<Calendar, Double> timeMap : dayMap.getValue().entrySet()) {
					if (campusDictionary.containsKey(entry.getKey())) {
						Calendar calendar = timeMap.getKey();
						//System.out.println(calendar.get(Calendar.HOUR_OF_DAY)+":"+calendar.get(Calendar.MINUTE) + "\t" + timeMap.getValue());
						String[] line = { campusDictionary.get(entry.getKey()), dayMap.getKey(), "" + calendar.getTimeInMillis(),
								"" + df.format(timeMap.getValue()) };
						allLines.add(line);
					}
				}
			}
		}
		writeCSV("allLines", allLines);
	}
	
	public static void writeCSV(String fileName, List<String[]> lines) throws Exception
	   {
	      String csv = fileName + "_final.csv";
	      CSVWriter writer = new CSVWriter(new FileWriter(csv));
	      writer.writeAll(lines);
	      writer.close();
	   }

}
